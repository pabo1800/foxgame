package se.miun.chrfin.foxgame;

import static net.finkn.foxgame.util.Role.FOX;
import static net.finkn.foxgame.util.Role.SHEEP;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.stream.Stream;

import net.finkn.foxgame.PerformanceTest;
import net.finkn.foxgame.adapters.EngineFactory;
import pabo.ai.foxgame.minimax.MinimaxFoxGameEngine;
import se.miun.chrfin.foxgame.com.Communicator;
import se.miun.chrfin.foxgame.exceptions.FoxgameSetupException;
import se.miun.chrfin.foxgame.logic.PlayerAgent;
import se.miun.chrfin.foxgame.setup.PlayerSetup;
import se.miun.chrfin.foxgame.setup.PlayerSetupParser;

/**
 * Starts a foxgame client by instantiating the player properties and
 * connecting to a server.
 *
 * @author Christoffer Fink
 * @author feldob
 */
public class Main {
    public static void main(String[] args) throws FoxgameSetupException, IOException {
        if (args.length > 0) {
            switch (args[0]) {
                case "test":
                    Stream.of(args)
                            .filter((arg) -> arg.equals("test"))
                            .findAny()
                            .ifPresent((s) -> {
                                test();
                                System.exit(0);
                            });
                default:
                    break;
            }
        }
        PlayerSetup setup = new PlayerSetupParser().parse(args);
        Communicator com = new Communicator(setup);
        MyFactory factory = new MyFactory();
        AiGameEngine ai = factory.getEngine(setup);
        PlayerAgent agent = new PlayerAgent(com, ai);
        agent.connectAndPlay();
    }

    private static void test() {
        EngineFactory factory = new MyFactory();

        // Tweaking each parameter.
        // result = PerformanceTest.test(factory)
        // .matches(10) // Run 5 matches per level (default is 10)
        // .random(123, 456)
        // .levels(1) // Runs levels 0 and 2 (default is 0, 1, and 2) (max 3)
        // .timeouts(200) // Use 200 ms timeout (default is 500 and 300)
        // .roles(SHEEP) // Only play as fox (default is FOX and SHEEP)
        // // .agents(new FoxGameEngineFactory()) // Want to play against
        // another
        // // agent? Put it
        // // here. Not used by default.
        // .run(.85); // Win threshold of 50% (default is 80%)
        // System.out.println("Result: " + result);

        testFox(factory);
        testSheep(factory);
    }

    static void testFox(EngineFactory factory) {
        boolean resultFox = PerformanceTest.test(factory)
                .matches(10)
                .levels(1)
                .timeouts(200)
                .roles(FOX)
                .run(1);

        try {
            assertTrue(resultFox, "Fox did not pass");
        } catch (AssertionError e) {
            System.out.println(e);
            return;
        }
    }

    static void testSheep(EngineFactory factory) {
        boolean resultSheep = PerformanceTest.test(factory)
                .matches(10)
                .levels(1)
                .timeouts(200)
                .roles(SHEEP)
                .run(1);
        try {
            assertTrue(resultSheep, "Sheep did not pass");
        } catch (AssertionError e) {
            System.out.println(e);
        }
    }

    static class MyFactory implements EngineFactory {
        @Override
        public AiGameEngine getEngine(String role) {
            var setup = new PlayerSetup("localhost", 6000, role, "pabo", "FOX");
            return getEngine(setup);
        }

        public AiGameEngine getEngine(PlayerSetup setup) {
            return new MinimaxFoxGameEngine(setup);
            // return new MinimaxEngine(setup);
        }

        @Override
        public String toString() {
            return "me"; // Or "AwesomeFoxgameEngine" if you want to. :)
        }
    }
}

