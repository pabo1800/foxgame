package pabo.ai.foxgame.minimax;

import java.util.Comparator;

public class Minimax<State, Action> {

    private static double startingAlpha = Double.NEGATIVE_INFINITY;
    private static double startingBeta = Double.POSITIVE_INFINITY;
    private static boolean log = false;

    private final EvalFunction<State> eval;
    private final TerminalTest<State> terminal;
    private final SuccessorFunction<State, Action> successorFunc;
    private int cutoff = 0;

    public Minimax(final EvalFunction<State> h, final TerminalTest<State> t,
            final SuccessorFunction<State, Action> s) {
        this.eval = h;
        this.terminal = t;
        this.successorFunc = s;
    }

    Node<State, Action> getMaxSuccessor(final State s, final int cutoff) {
        this.cutoff = cutoff;
        return this.successorFunc.successorsReverse(s)
                .stream()
                // .parallel()
                .map((successor) -> {
                    final double utility = maximize(successor.state, startingAlpha, startingBeta, 1);
                    return new Child(successor, utility);
                })
                .max(Comparator.comparing(Child::utility))
                .get().successor;
    }


    Double maximize(final State state, double alpha, final double beta, final int depth) {
        String padding = " ".repeat(depth);
        log(padding + "Maximize (cutoff = " + cutoff + ")");
        if (terminal.test(state) || depth >= cutoff) {
            log(padding + "Utility " + eval.eval(state) + " Terminal? " + terminal.test(state));
            return eval.eval(state);
        }
        double maxValue = Double.NEGATIVE_INFINITY;
        for (final var successor : successorFunc.successorsReverse(state)) {
            final double value = minimize(successor.state, alpha, beta, depth + 1);
            log(padding + "value: " + value);
            maxValue = Math.max(maxValue, value);
            alpha = Math.max(alpha, maxValue);
            if (beta <= alpha) {
                log(padding + "Prune");
                break;
            }
        }
        log(padding.replaceAll(" ", "-") + "Chose " + maxValue);
        return maxValue;
    }

    Node<State, Action> getMinSuccessor(final State s, final int cutoff) {
        this.cutoff = cutoff;
        return this.successorFunc.successors(s)
                .stream()
                // .parallel()
                .map((successor) -> {
                    final double utility = minimize(successor.state, startingAlpha, startingBeta, 1);
                    return new Child(successor, utility);
                })
                .min(Comparator.comparing(Child::utility))
                .get().successor;
    }

    Double minimize(final State state, final double alpha, double beta, final int depth) {
        String padding = " ".repeat(depth);
        log(padding + "Minimize (cutoff = " + cutoff + ")");
        if (terminal.test(state) || depth >= cutoff) {
            log(padding + "Utility " + eval.eval(state) + " Terminal? " + terminal.test(state));
            return eval.eval(state);
        }
        double minValue = Double.POSITIVE_INFINITY;
        for (final var successor : successorFunc.successors(state)) {
            final double value = maximize(successor.state, alpha, beta, depth + 1);
            log(padding + "value: " + value);
            minValue = Math.min(minValue, value);
            beta = Math.min(beta, minValue);
            if (beta <= alpha) {
                log(padding + "Prune");
                break;
            }
        }
        log(padding.replaceAll(" ", "-") + "Chose " + minValue);
        return minValue;
    }

    void log(String msg) {
        if (log) {
            System.out.println(msg);
        }
    }

    class Child {
        public final Node<State, Action> successor;
        public final double utility;

        Child(final Node<State, Action> successor, final double utility) {
            this.successor = successor;
            this.utility = utility;
        }

        double utility() {
            return this.utility;
        }
    }
}
