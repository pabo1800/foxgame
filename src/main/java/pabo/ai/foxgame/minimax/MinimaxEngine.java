package pabo.ai.foxgame.minimax;

import java.util.*;
import java.util.concurrent.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ch.rfin.ai.games.Game;
import ch.rfin.foxgame.Foxgame;
import ch.rfin.foxgame.Role;
import ch.rfin.foxgame.rules.State;
import se.miun.chrfin.foxgame.AiGameEngine;
import se.miun.chrfin.foxgame.com.GameStatus;
import se.miun.chrfin.foxgame.setup.PlayerSetup;

public class MinimaxEngine implements AiGameEngine {

    private final Minimax<State, String> minimax;
    private State state;
    private Game<State, String> game;
    private final BiFunction<State, Integer, String> bestMove;
    private final ExecutorService exec = Executors.newFixedThreadPool(2);
    private Logger logger = new Logger(Logger.Level.ERROR);

    public MinimaxEngine(PlayerSetup setup) {
        this(setup.playerRole);
    }

    public MinimaxEngine(String role) {
        this.game = new Foxgame();
        this.state = game.getRoot();
        this.minimax = new Minimax<State, String>(
                this::eval,
                this::terminal,
                getSuccesorFunction());
        if (getRole(role).equals(Role.FOX)) {
            bestMove = this::maxMove;
        } else {
            bestMove = this::minMove;
        }
    }

    private Role getRole(String role) {
        switch (role) {
            case "FOX":
                return Role.FOX;
            case "SHEEP":
                return Role.FOX;
            default:
                throw new RuntimeException("Invalid role: " + role);
        }
    }

    double eval(State s) {
        if (game.terminal(s)) {
            return game.utilityOf(s) * 10000D;
        }
        return heuristic(s);
    }

    double heuristic(State s) {
        Double f = (double) s.getFoxes().size();
        Double g = (double) s.getSheep().size();
        Double h = s.getSheep().stream()
                .mapToInt((p) -> p.getY())
                .mapToDouble(Double::valueOf)
                .sum();
        return 50 * f + h - g;
    }

    boolean terminalMove(String move) {
        var newState = game.transition(state, move);
        return terminal(newState);
    }

    boolean terminal(State s) {
        return game.terminal(s);
    }

    @Override
    public String getMove(GameStatus status) {
        long start = now();
        long ts = status.timeSlice - 40;
        long millisLeft = ts - elapsedTime(start);
        String move = null;
        int cutoff = 1;
        for (; millisLeft > 0; cutoff++) {
            millisLeft = ts - elapsedTime(start);
            var result = execBestMove(state, cutoff, millisLeft);
            if (result.isEmpty()) {
                break;
            }
            move = result.get();
        }
        logInfo("Ran " + (cutoff - 1) + " iterations in " + elapsedTime(start) + "ms");
        return move;
    }

    Optional<String> execBestMove(State s, int cutoff, long msTimeOut) {
        var future = exec.submit(() -> {
            return bestMove.apply(s, cutoff);
        });
        try {
            String value = future.get(msTimeOut, TimeUnit.MILLISECONDS);
            return Optional.of(value);
        } catch (InterruptedException e) {
            logError("ERROR: future.get() was interrupted");
            return Optional.empty();
        } catch (TimeoutException e) {
            return Optional.empty();
        } catch (ExecutionException e) {
            logError("Generating move caused an exception: " + e.getMessage());
            throw new RuntimeException("Execution caused an exception!");
        } finally {
            logDebug("Cancelling future");
            future.cancel(true);
        }
    }


    SuccessorFunction<State, String> getSuccesorFunction() {
        return new SuccessorFunction<State, String>() {

            @Override
            public Collection<Node<State, String>> successors(State s) {
                return getSuccessors(s)
                        .sorted(Comparator.comparing((node) -> eval(node.state)))
                        .collect(Collectors.toList());
            }

            @Override
            public Collection<Node<State, String>> successorsReverse(State s) {
                return getSuccessors(s)
                        .sorted(Comparator.comparing((node) -> -1D * eval(node.state)))
                        .collect(Collectors.toList());
            }
        };
    }

    Stream<Node<State, String>> getSuccessors(State s) {
        return game.possibleActionsIn(s)
                .stream()
                .map((move) -> {
                    State newState = game.transition(state, move);
                    return new Node<State, String>(newState, move);
                });
    }


    void log(String msg) {
        logger.log(msg);
    }

    void logDebug(String msg) {
        logger.logDebug(msg);
    }

    void logError(String msg) {
        logger.logError(msg);
    }

    void logInfo(String msg) {
        logger.logInfo(msg);
    }

    long now() {
        return System.currentTimeMillis();
    }

    long elapsedTime(long start) {
        return now() - start;
    }

    public String maxMove(State s, int cutoff) {
        return minimax.getMaxSuccessor(s, cutoff).move;
    }

    public String minMove(State s, int cutoff) {
        return minimax.getMinSuccessor(s, cutoff).move;
    }

    @Override
    public void updateState(String move) {
        state = game.transition(state, move);
    }

    @Override
    public String getPlayerName() {
        return "Patrik";
    }
}
