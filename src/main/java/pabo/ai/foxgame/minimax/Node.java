package pabo.ai.foxgame.minimax;

public class Node<State, Action> {
    public final State state;
    public final Action move;

    public Node(State s, Action move) {
        this.state = s;
        this.move = move;
    }
}
