package pabo.ai.foxgame.minimax;

@FunctionalInterface
public interface TerminalTest<State> {
    boolean test(State s);
}
