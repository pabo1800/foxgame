package pabo.ai.foxgame.minimax;

import java.util.*;
import java.util.concurrent.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import ch.rfin.ai.games.Game;
import ch.rfin.foxgame.Foxgame;
import ch.rfin.foxgame.Pos;
import ch.rfin.foxgame.rules.State;
import se.miun.chrfin.foxgame.AiGameEngine;
import se.miun.chrfin.foxgame.com.GameStatus;
import se.miun.chrfin.foxgame.setup.PlayerSetup;

/**
 * @author Christoffer Fink
 */
public class MinimaxFoxGameEngine implements AiGameEngine {

    enum LogLevel {
        ALWAYS, ERROR, INFO, DEBUG
    }

    final static LogLevel logLevel = LogLevel.ERROR;

    final static int startingAlpha = Integer.MIN_VALUE;
    final static int startingBeta = Integer.MAX_VALUE;
    final static ExecutorService exec = Executors.newSingleThreadExecutor();


    private final Game<State, String> game = new Foxgame();
    private State currentState = game.getRoot();
    private final String playerRole;
    final ConcurrentHashMap<State, SearchResult> transpositions = new ConcurrentHashMap<>(100);

    public MinimaxFoxGameEngine(PlayerSetup setup) {
        this.playerRole = setup.playerRole;

        if (playerRole.equals("FOX")) {
        } else if (playerRole.equals("SHEEP")) {
        } else {
            throw new RuntimeException("No valid role specified");
        }
        log("============================\n"
                + "    Playing as i " + playerRole + "!\n"
                + "============================",
                LogLevel.ALWAYS);
    }

    /**
     * Return a move of the form "x1,y1 x2,y2".
     */
    @Override
    public String getMove(GameStatus status) {
        final var start = now();
        long ts = status.timeSlice - 30;
        long millisLeft = ts - elapsedTime(start);

        final var state = currentState;
        String bestMove = null; // currentNode.firstPossibleMove();
        log("\n\nStarting new search", LogLevel.INFO);
        for (int cutoff = 1; millisLeft > 0; cutoff++) {
            final var future = findBestNode(state, cutoff);
            if(cutoff == 1) {
                bestMove = fallbackMove(state);
            }
            millisLeft = ts - elapsedTime(start);
            final var result = get(future, millisLeft);
            if (result.isEmpty()) {
                if (cutoff == 1) {
                    //logNoSuccessfulIteration(state, elapsedTime(start));
                }
                break;
            }
            bestMove = result.get().move;
        }
        log("Returned move " + bestMove, LogLevel.DEBUG);
        return bestMove;
    }

    <T> Optional<T> get(Future<T> future, long millisLeft) {
        try {
            var result = future.get(millisLeft, TimeUnit.MILLISECONDS);
            if (result == null) {
                throw new InterruptedException();
            }
            return Optional.of(result);
        } catch (InterruptedException e) {
            log("ERROR: future.get() was interrupted", LogLevel.ERROR);
            return Optional.empty();
        } catch (TimeoutException e) {
            return Optional.empty();
        } catch (ExecutionException e) {
            log("Generating move caused an exception: " + e.getMessage(), LogLevel.ERROR);
            throw new RuntimeException("Execution caused an exception!");
        } finally {
            log("Cancelling future", LogLevel.DEBUG);
            future.cancel(true);
        }
    }

    String fallbackMove(State s) {
        var successors = successors(s).stream().collect(Collectors.toList());
        Collections.shuffle(successors, new Random());
        return successors.stream().findFirst().get().move; 
    }

    Future<Successor> findBestNode(State s, int cutoff) {
        if (s.getCurrentPlayer().toString().equals("FOX")) {
            return findMaxNode(s, cutoff);
        }
        return findMinNode(s, cutoff);
    }

    Future<Successor> findMaxNode(State s, int cutoff) {
        return exec.submit(() -> {
            return successors(s)
                    .stream()
                    .parallel()
                    .map((successor) -> {
                        var utility =
                                maximize(successor.state, startingAlpha, startingBeta, 1, cutoff);
                        return new Child(successor, utility);
                    })
                    .max(Comparator.comparing(Child::utility))
                    .get().successor;
        });
    }

    Future<Successor> findMinNode(State s, int cutoff) {
        return exec.submit(() -> {
            return successors(s)
                    .stream()
                    .parallel()
                    .map((successor) -> {
                        var utility =
                                minimize(successor.state, startingAlpha, startingBeta, 1, cutoff);
                        return new Child(successor, utility);
                    })
                    .min(Comparator.comparing(Child::utility))
                    .get().successor;
        });
    }

    int maximize(final State state, int alpha, final int beta, final int depth, final int cutoff) {
        if (terminal(state)) {
            return utility(state);
        }
        if (depth >= cutoff) {
            var h = heuristic(state);
            log("Returning heuristic: " + h, LogLevel.INFO);
            return h;
        }
        var maxValue = Integer.MIN_VALUE;
        for (final var successor : successors(state)) {
            int value = minimize(successor.state, alpha, beta, depth + 1, cutoff);
            maxValue = Math.max(maxValue, value);
            alpha = Math.max(alpha, value);
            if (beta <= alpha) {
                log("Pruning Min nodes", LogLevel.DEBUG);
                break;
            }
        }
        return maxValue;
    }

    int minimize(final State state, final int alpha, int beta, final int depth, final int cutoff) {
        if (terminal(state)) {
            return utility(state);
        }
        if (depth >= cutoff) {
            return heuristic(state);
        }
        var minValue = Integer.MAX_VALUE;
        for (final var successor : successors(state)) {
            int value = maximize(successor.state, alpha, beta, depth + 1, cutoff);
            minValue = Math.min(minValue, value);
            beta = Math.min(beta, value);
            if (beta <= alpha) {
                log("beta cutoff", LogLevel.DEBUG);
                break;
            }
        }
        return minValue;
    }


    boolean terminal(State state) {
        return game.terminal(state);
    }

    int utility(State state) {
        return (int) game.utilityOf(state) * 100000;
    }

    State transition(State s, String move) {
        return game.transition(s, move);
    }

    Collection<Successor> successors(State state) {
        return game.possibleActionsIn(state)
                .stream()
                .map((move) -> new Successor(move, transition(state, move)))
                .collect(Collectors.toList());
    }

    int heuristic(State s) {
        var foxes = s.getFoxes().size();
        var sheep = s.getSheep().size();
        var dist = s.getSheep()
                .stream()
                .mapToInt(Pos::getY)
                .sum();
        return 50 * foxes + dist - sheep;
    }

    void logNoSuccessfulIteration(State state, long duration) {
        var msg = String.format("Reached depth %d in %d ms \n\nState (%d successors) %s\n",
                0, duration, successors(state).size(), state.toString());
        log(msg, LogLevel.ALWAYS);

    }

    long elapsedTime(long start) {
        return now() - start;
    }

    long now() {
        return System.currentTimeMillis();
    }

    @Override
    public void updateState(String move) {
        log("Updated state with move: " + move, LogLevel.INFO);
        currentState = transition(currentState, move);
    }

    @Override
    public String getPlayerName() {
        return "Patrik";
    }

    private void log(String msg, final LogLevel lvl) {
        if (lvl.ordinal() <= logLevel.ordinal()) {
            System.out.println(msg);
        }
    }

    class Child {
        public final Successor successor;
        public final int utility;

        Child(Successor s, int utility) {
            this.successor = s;
            this.utility = utility;
        }

        int utility() {
            return utility;
        }
    }

    class Successor {
        public final String move;
        public final State state;

        Successor(String move, State state) {
            this.move = move;
            this.state = state;
        }
    }

    class SearchResult {
        final int searchDepth;
        final int utility;

        SearchResult(int utility, int searchDepth) {
            this.searchDepth = searchDepth;
            this.utility = utility;
        }
    }
}
