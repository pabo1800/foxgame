package pabo.ai.foxgame.minimax;

import java.util.Collection;

public interface SuccessorFunction<State, Action> {
    Collection<Node<State, Action>> successors(State s);
    Collection<Node<State, Action>> successorsReverse(State s);
}
