package pabo.ai.foxgame.minimax;

@FunctionalInterface
interface EvalFunction<State> {
    Double eval(State s);
}
