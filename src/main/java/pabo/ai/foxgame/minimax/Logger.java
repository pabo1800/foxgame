package pabo.ai.foxgame.minimax;

class Logger {

    enum Level {
        ALWAYS, ERROR, INFO, DEBUG
    }

    private Level logLevel;

    public Logger(Level lvl) {
        this.logLevel = lvl;
    }

    public void log(String msg) {
        log(msg, Level.ALWAYS);
    }

    public void logError(String msg) {
        log(msg, Level.ERROR);
    }

    public void logInfo(String msg) {
        log(msg, Level.INFO);
    }

    public void logDebug(String msg) {
        log(msg, Level.DEBUG);
    }

    public void log(String msg, final Level lvl) {
        if (lvl.ordinal() <= logLevel.ordinal()) {
            System.out.println(msg);
        }
    }
}
